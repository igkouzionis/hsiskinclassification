function [indx, ssw, sw, sb] = valid_clusterIndex(data, labels)

[nr, nc] = size(data);
k = max(labels);
[st, sw, sb, S, Sinter] = valid_sumsqures(data, labels, k);
ssw = trace(sw);
ssb = trace(sb);

if k>1
  CH = ssb/(k-1); % Calinski-Harabasz
  
 % Davies-Bouldin 
  R = NaN * zeros(k);
  dbs = zeros(1,k);
  for i = 1:k
    for j = i+1:k
      R(i,j) = (S(i) + S(j))/Sinter(i,j);
    end
    dbs(i) = max(R(i,:));
  end
  
  % Davies-Bouldin for all clusters
  db = dbs(isfinite(dbs));
  
  DB = mean(db); 
 
else
  CH = ssb; 
  DB = NaN;
end

% Calinski-Harabasz
CH = (nr-k)*CH/ssw; 

% Krzanowski and Lai
KL = (k^(2/nc))*ssw; 

indx = [DB CH KL]'; 
