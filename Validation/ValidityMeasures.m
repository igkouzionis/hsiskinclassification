%%
clear all;
clc;

temp=sequence;
size_temp = size(temp);
temp2 = reshape(permute(temp,[1 2 3]),[size_temp(1)*size_temp(2) size_temp(3)]);

data = temp2;

[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels);
inter = min(cinter(cinter>0));
intra = min(cintra/max(labels));

BIC = bic(data, labels, centroids);

save([str3 '_Validation'],'CH','DB','Han','inter','intra','KL','ssb','ssw')


k = max(labels);
distM = squareform(pdist(data));
DI = dunns(k,distM,labels);

Sil = avgSilhouette( data,labels,k );

[Sil, fig] = silhouette(data,labels);
Sil = mean(Sil);             % mean Silhouette
imshow(fig);
save('Silhouette','Sil')
%% crashes due to RAM
% [Dist,dmax] = similarity_euclid(data);
% S = ind2cluster(labels);
% % weighted inter/intra ratio
% [Hom, Sep, wtertra] ...           
%     = valid_internal_intra(Dist, S, dmax);

%% Between clustering

RandIndex_Agglom = clustereval(a, b, 'ri');
RandIndex_FCM = clustereval(a, b, 'ri');
RandIndex_GMM = clustereval(a, b, 'ri');
RandIndex_ISODATA = clustereval(a, b, 'ri');
RandIndex_SOM = clustereval(a, b, 'ri');
save('Comparison_RandIndex_Art', 'RandIndex_Agglom', 'RandIndex_FCM', 'RandIndex_GMM', 'RandIndex_ISODATA', 'RandIndex_SOM')
clear all;clc;
