% Number of clusters
N = 8;
% Distance metric to be used
Rd = 'euclidean';
NC = 2:N;
AR = zeros(1,N);
Rand = zeros(1,N);
Mirkin = zeros(1,N);
Hubert = zeros(1,N);
Sil = zeros(1,N);
DB = zeros(1,N);
CH = zeros(1,N);
KL = zeros(1,N);
Ha = zeros(1,N);
Hom = zeros(1,N);
Sep = zeros(1,N);
wtertra = zeros(1,N);

Re = strcmp(Rd, 'euclidean');
% Internal validity indices when true labels are unknown
for i = NC
   R = silhouette(data, labels(:,i), Rd);
   % Average Silhouette
   Sil(i) = mean(R);        
   % Davies-Bouldin, Calinski-Harabasz, Krzanowski-Lai, Hartigan
   [DB(i), CH(i), KL(i), Ha(i), ST] = ...
       valid_internal_deviation(data,labels(:,i), Re);
end

kl = KL(NC);
ha = Ha(NC);
nl = length(NC);
S = trace(ST);
kl = [S kl];
ha = [S ha];
R = abs(kl(1:nl)-kl(2:nl+1));
S = [R(2: end) R(end)];
kl = R./S;
kl(nl) = kl(nl-1);
R = ha(1:nl)./ha(2:nl+1);
ha = (R-1).*(nrow-[NC(1)-1 NC(1:nl-1)]-1); 
KL(NC) = kl;
Ha(NC) = ha;
