classdef DBSCAN_ < Initial_Function
    
    properties (GetAccess = 'public', SetAccess = 'public')

    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        
        labels_; % Labels of each point
        eps_;
    end
    
    methods
        function obj = DBSCAN_(params)
            if nargin > 0
                obj.set_params(params)
            end
        end
        
        function fit(obj, X, MinPts, ~)
            
            % My function for DBSCAN      
            [idx,c,eps] = DBSCAN2(X, MinPts);
%             [idx,eps] = DBSCAN(X, MinPts);
            obj.labels_ = idx;
            obj.eps_ = eps;
        end
        
        function labels = fit_predict(obj, X, ~)
            obj.fit(X);
            labels = obj.predict(X);
        end
        
        function X_new = fit_transform(obj, X, ~)
            obj.fit(X);
            X_new = obj.transform(X);
        end
        
        function labels = predict(obj, X, cluster_centers, ~)
            X_new = obj.transform(X, cluster_centers);
            [~,labels] = min(X_new,[],2);
        end
        
        function X_new = transform(obj, X, cluster_centers, ~)
            X_new = dist2(X, cluster_centers);
        end
    end
end
