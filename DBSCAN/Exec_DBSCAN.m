function [labels, centroids] = Exec_DBSCAN(data1, data2, minPoints)

% data1: low resolution hypercube m*n*d (spatial dimensions m, n and spectral dimension d)
%
% data2: high resolution hypercube m*n*d (spatial dimensions m, n and spectral dimension d)
%
% minPoints: minimum points to form a cluster

temp1 = data1;
size_temp1 = size(temp1);
temp1 = reshape(permute(temp1,[1 2 3]),[size_temp1(1)*size_temp1(2) size_temp1(3)]);

temp2 = data2;
size_temp2 = size(temp2);
temp2 = reshape(permute(temp2,[1 2 3]),[size_temp2(1)*size_temp2(2) size_temp2(3)]);

%% Perform PCA

[U,mu,vars] = pca(temp2);
pc1 = reshape(U(:,1), [size_temp2(1) size_temp2(2)]);
pc2 = reshape(U(:,2), [size_temp2(1) size_temp2(2)]);
pc3 = reshape(U(:,3), [size_temp2(1) size_temp2(2)]);

imwrite(pc1.*255, strcat('PC1.png'));
imwrite(pc2.*255, strcat('PC2.png'));
imwrite(pc3.*255, strcat('PC3.png'));

PCAImage = cat(3, pc1, pc2, pc3);
imwrite(PCAImage.*255, strcat('PCA_RGB.png'));

%% Perform DBSCAN

MinPts = minPoints;

tic

clf = DBSCAN_();
clf.fit(temp1, MinPts); % training step
Eps = clf.eps_;
labels = clf.labels_;
n_cluster = max(labels); % number of clusters
centroids = zeros(n_cluster, size_temp1(3));
% calculate centroids
for j = 1:n_cluster
        centroids(j,:) = mean(temp1(labels(:,1) == j,:));
end
y_est = clf.predict(temp2, centroids); % mapping step
labels = y_est;

time = toc;

%% Save labels and centroids

xlswrite(['DBSCAN_' int2str(n_cluster) '_Centroids.xlsx'], centroids.*255);
save(['DBSCAN_' int2str(n_cluster) '_Labels'], 'labels')
str3 = ['DBSCAN_' int2str(n_cluster)];

%% Produce thematic map

% IDX = reshape(labels,[size_temp2(1) size_temp2(2)]);
% [imgOut] = hyperConvert2Colormap(IDX, jet);
% imshow(imgOut);

%% Save the pseudocolor map 

% strSave=['DBSCAN_' 'Clusters' int2str(n_cluster) '_' 'Eps' int2str(Eps) '_' 'MinPts' int2str(MinPts) '_' 'ResIn' int2str(size_temp1(1)) '_' int2str(size_temp1(2)) '_' 'ResOut' int2str(size_temp2(1)) '_' int2str(size_temp2(2)) '_' int2str(time) 'secs' '_' int2str(str2num(datestr(now,'ddmmyyHHMMSS')))];
% imwrite(imgOut, [strSave '.png'], 'png');
