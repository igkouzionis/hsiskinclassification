function [assignments,C,EPS] = DBSCAN2(X,minpts)
  C = 0;
  EPS = epsilon(X,minpts);
  assignments = zeros(size(X,1),1);
  clustered = zeros(size(X,1),1);
  for i=1: size(X,1)
    if(clustered(i)==1)
      continue;
    end
    clustered(i)=1;
    isneighbour = [];
    neighbourcount = 0;
    for j=1: size(X,1)
      dist = sqrt(sum((X(i,:)-X(j,:)).^2));
      if(dist<EPS)
        neighbourcount = neighbourcount+1;
        isneighbour = [isneighbour j];
      end
    end
    if(neighbourcount<minpts)
      continue;
    else
      C=C+1;
      assignments(i) = C;
      for k=isneighbour
        if(clustered(k)==0)
          clustered(k) = 1;
          isneighbour = [];
          neighbourcount = 0;
          for j=1: size(X,1)
            dist = sqrt(sum((X(k,:)-X(j,:)).^2));
            if(dist<EPS)
              neighbourcount=neighbourcount+1;
              isneighbour = [isneighbour j];
            end
            end
          if(neighbourcount>=minpts)
            isneighbour = [isneighbour isneighbour];
          end
          end
        assignments(k) = C;
          end
          end
  end
          
  function [Eps] = epsilon(x,k)

[m, n] = size(x);

Eps = ((prod(max(x)-min(x))*k*gamma(.5*n+1))/(m*sqrt(pi.^n))).^(1/n);
