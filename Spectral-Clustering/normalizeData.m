function normalizedData = normalizeData(Data)

a = 0;
b = 1;

minData = min(Data, [], 2);
maxData = max(Data, [], 2);

r = (a-b) ./ (minData - maxData);
s = a - r .* minData;

normalizedData = repmat(r, 1, size(Data, 2)) .* Data + repmat(s, 1, size(Data, 2));
