function [ M ] = simGaussian( M, sigma )

M = exp(-M.^2 ./ (2*sigma^2));

end
