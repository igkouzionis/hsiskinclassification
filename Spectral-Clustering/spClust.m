function D = spClust(temp, NumClusts)
ClusterIm = [];
k = NumClusts;
Neighbors = 10; 
roundColors = 1;
roundDigits = 2; 
saveData = 0; 
markEdges = 0;
Img = temp;
[m, n, d] = size(Img);
b = 5;
[Y, U, Lambda, Mu] = PCAbyDG(Img,b);
Data = Y;
Data = reshape(Data, 1, m * n, []);
if d >= 2
    Data = (squeeze(Data))';
end
% convert to double and normalize to [0,1]
Data = double(Data);
Data = normalizeData(Data);
% Find unique colors
if isequal(roundColors, 1)
    fac = 10^roundDigits;
    rData = round(Data * fac) / fac;
else
    rData = Data;
end
[~, ind, order] = unique(rData', 'rows', 'R2012a');
% crop data
Data = Data(:, ind);
% now for the clustering
fprintf('Creating Similarity Graph...\n');
SimGraph = SimGraph_NearestNeighbors(Data, Neighbors, 1);

try
    comps = graphconncomp(SimGraph, 'Directed', false);
    fprintf('- %d connected components found\n', comps);
end

fprintf('Clustering Data...\n');
C = SpectralClustering(SimGraph, k, 3);

%ConnImg = SpectralClustering(SimGraph, k, 2);

% convert and restore full size
D = convertClusterVector(C);
D = D(order);

% reshape indicator vector into m-by-n
% S = reshape(D, m, n);

% plot image
% set(gca, 'Position', [0 0 1 1], 'Units', 'Normalized');
% ClusterIm = S;
