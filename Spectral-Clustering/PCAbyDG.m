function [Y, U, Lambda, Mu] = PCAbyDG(X, NComps);

Sx    = size(X);
NDims = min(4,length(Sx)); 
switch NDims
    case 1
        error('Error: Not enough Dimensions');
    case 3
        N = Sx(1)*Sx(2);
        B = Sx(3);
        X = shiftdim(X, 2);
        X = reshape(X, [B, N]);
    case 4
        error('Error: Too many Dimensions');
end

[B,N] = size(X);
Mu    = mean(X, 2);
BigMu = repmat(Mu, [1, N]);
Xz    = X-BigMu;

C = (1/(N-1))*Xz*Xz';

[U, Lambda] = eig(C);

Lambda  = diag(Lambda);
[Lambda,Index]= sort(Lambda,'descend'); % to sort eigenvalues in decresing order
 U= U(:,Index);
 U=U';

Lambda  = diag(Lambda);


Y = U*Xz;
Y = Y(1:NComps, :);

Y = Y';

end
