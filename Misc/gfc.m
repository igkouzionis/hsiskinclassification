function gfc_value = gfc(X,Y)
gfc_value = mean(sum(abs(X.*Y),1) ./ (sum(X.^2,1).^0.5 .* sum(Y.^2,1) .^0.5));
