function colors = sameCmap_2(centroidsRef, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec)
    K = size(centroidsRef,1);
    for i=1:K
        angle1=SID(centroidsRef(i,:),comp_spec); 
        angle2=SID(centroidsRef(i,:),dysp_spec); 
        angle3=SID(centroidsRef(i,:),junc_spec); 
        angle4=SID(centroidsRef(i,:),mel_spec); 
        angle5=SID(centroidsRef(i,:),norm_spec); 
        minAngle = min([angle1,angle2,angle3,angle4,angle5]);
            if minAngle==angle1
                colors(i,:)=[0 0 0]; % black - compound
            elseif minAngle==angle2
                colors(i,:)=[128 0 0]; % red - dysplastic
            elseif minAngle==angle3
                colors(i,:)=[0 128 0]; % green - junctional
            elseif minAngle==angle4
                colors(i,:)=[0 0 128]; % blue - melanoma
            elseif minAngle==angle5
                colors(i,:)=[128 128 0]; % yellow - normal
            end
    end