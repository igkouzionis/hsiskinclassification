%% Import hyperspectral image stack as data matrix D
% Width of D is the number of spectral channels
% Height of D is the number of pixels
% clear;
% clc;
% addpath('\hsImages');
% Define size of the image
function C = Exec_Unmixing(data, endmembers)
size_x = size(data,1);
size_y = size(data,2);
size_lambda = size(data,3);
D = reshape(data, size_x*size_y,size_lambda);

% y = zeros(size_x,size_y,size_lambda);
% D = zeros(size_x*size_y, size_lambda);
% 
% for i = 1:size_lambda
%     temp = ['Image',num2str(i),'.png'];
%     img = imread(temp);
%     temp_column = reshape(img,size_x*size_y,1);
%     y(:,:,i) = img;
%     D(:,i) = temp_column;
% end
% clear size_x size_y size_lambda temp temp_column i img;
% rmpath('Data/CElegans');

%% Run LASSO
lambda = 0.6e1;
copt = C_ML_Lasso( data, endmembers',lambda);
sopt = endmembers;

%% Plot the results

% color_map=GetColorMap(size(copt,3));
% C = zeros(size(copt,1), size(copt,2), size(copt,3));
% % for lasso use
% for i = 1:size(copt,3)
%     Comp{i} = copt(:,:,i);
%     Comp{i} = imadjust(uint8(255.*(Comp{i} - min(Comp{i}(:) ))/(max(Comp{i}(:))-min(Comp{i}(:) ))));
%     rgbComp{i} = hyperConvert2Colormap(Comp{i}, color_map);
% %     C(:,:,i) = C(:,:,i) + rgbComp{i}; 
% end
% 
% C = rgbComp{1} + rgbComp{2} + rgbComp{3} + rgbComp{4};

Comp1 = copt(:,:,1);
Comp2 = copt(:,:,2);
Comp3 = copt(:,:,3);
Comp4 = copt(:,:,4);

Comp1 = imadjust(uint8(255.*(Comp1 - min(Comp1(:) ))/(max(Comp1(:))-min(Comp1(:) ))));
Comp2 = imadjust(uint8(255.*(Comp2 - min(Comp2(:) ))/(max(Comp2(:))-min(Comp2(:) ))));
Comp3 = imadjust(uint8(255.*(Comp3 - min(Comp3(:) ))/(max(Comp3(:))-min(Comp3(:) ))));
Comp4 = imadjust(uint8(255.*(Comp4 - min(Comp4(:) ))/(max(Comp4(:))-min(Comp4(:) ))));
Comp5 = imadjust(uint8(255.*(Comp5 - min(Comp5(:) ))/(max(Comp5(:))-min(Comp5(:) ))));

rgbComp1 = cat(3, Comp1*1, Comp1*0, Comp1*1); % magenta for compound
rgbComp2 = cat(3, Comp2*0, Comp2*1, Comp2*1); % cyan for dysplastic
rgbComp3 = cat(3, Comp3*1, Comp3*0, Comp3*0); % red for junctional
rgbComp4 = cat(3, Comp4*0, Comp4*1, Comp4*0);% green for melanoma
rgbComp5 = cat(3, Comp5*0, Comp5*0, Comp5*1);% blue for normal
C_disp = rgbComp1+rgbComp2+rgbComp3+rgbComp4;
% 
imshow(C_disp,'InitialMagnification',150);

Spec1 = sopt(1,:);
Spec2 = sopt(2,:);
Spec3 = sopt(3,:);
Spec4 = sopt(4,:);
Spec5 = sopt(5,:);
subplot(1,3,2);
plot(Spec1,'m','Linewidth',1.5);
hold on;
plot(Spec2,'c','Linewidth',1.5);
plot(Spec3,'r','Linewidth',1.5);
plot(Spec4,'g','Linewidth',1.5);
plot(Spec5,'b','Linewidth',1.5);
xlabel 'Channel';
ylabel 'Int. (a.u.)';
legend('Compound','Dysplastic','Junctional','Melanoma','Normal')
pbaspect([2 1 1]);
