function rmse_value = my_rmse(A, B)
B = adjust_and_normalize(A, B);
C = (A - B) .^ 2;
rmse_value = mean(C(:)).^0.5;

function B = adjust_and_normalize(A, B)
B(B < 0) = 0;
C = mean(A, 1); 
D = mean(B, 1) + 0.00001; %avoid divided by zero

B = B .* repmat(C./D, size(A,1), 1);
