classdef Initial_Function < handle
    
    properties (GetAccess = 'public', SetAccess = 'public')
    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
    end
    
    methods
        function names = get_param_names(obj)
            names = fieldnames(obj);
        end

        function params = get_params(obj)
            params = struct;
            for fn = fieldnames(obj)'
                params.(fn{1}) = obj.(fn{1});
            end
        end

        function set_params(obj,params)
            % copy valid parameters
            if (nargin > 0) 
                for fn = fieldnames(params)'
                    if ismember(fn{1},fieldnames(obj))
                        values = params.(fn{1});
                        obj.(fn{1}) = values;
                    else
                        warning('Unknown parameter: %s.',fn{1});
                    end
                end
            end
        end
    end
end
