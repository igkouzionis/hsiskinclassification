%% Classify the test data
function est_label = testing(svms, test_data, training_label, name, verbose)
num_test_data = size(test_data, 1);
classes = unique(training_label);
num_classes = numel(classes);
score = nan(num_test_data, num_classes);
classes = unique(training_label);
num_classes = numel(classes);
if ~exist('verbose', 'var')
    verbose = 0;
end
for k=1:num_classes
    if verbose
        fprintf(['Classifying with Classifier ', num2str(classes(k)) ' of ', num2str(num_classes), '\n']);
    end
    [~, temp_score] = predict(svms{k}, test_data);
    score(:, k) = temp_score(:, 2);
end;

[~, est_label] = max(score, [], 2);
save([name '_labels'], 'est_label')

end


