function newCmap = sameCmap(centroidsRef, centroidsTest, color_map)
    % Find the matching between centroids of two clustering methods
    centroidsRef(isnan(centroidsRef))=0;
    centroidsTest(isnan(centroidsTest))=0;
    K = size(centroidsRef,1);
    for i=1:K
        for j=1:K
            % Compare each centroid of FCM with the centroids of Agglomerative
            dif(i,j) = euclideanDistance(centroidsTest(i,:),centroidsRef(j,:));
%             dif(i,j) = sqrt((centroidsTest(i,:)-centroidsRef(j,:)) * (centroidsTest(i,:)-centroidsRef(j,:))');
%             dif(i,j) = SID(centroidsTest(i,:),centroidsRef(j,:));
        end
        dif(isnan(dif))=0;
        c = min(dif(i,:));
        [row, col] = find(dif == c);
        mtc(i,:) = [row(1), col(1)];
    end

    % Use same colour for similar cluster centroids
    for i=1:size(mtc,1)
        newCmap(i,:) = color_map(mtc(i,2),:);
    end

end
