function classif=label2color(label)

[w h]=size(label);

im=zeros(w,h,3);

map=[0 0 255;0 255 0;255 0 0];
for i=1:w
    for j=1:h
        switch(label(i,j))
            case(1)
                im(i,j,:)=uint8(map(1,:));
            case(2)
                im(i,j,:)=uint8(map(2,:));
            case(3)
                im(i,j,:)=uint8(map(3,:));
        end
    end
end
       
im=uint8(im);
classif=uint8(zeros(w,h,3));
classif(:,:,1)=im(:,:,1);
classif(:,:,2)=im(:,:,2);
classif(:,:,3)=im(:,:,3);
%imwrite(classif,name);