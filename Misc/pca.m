function [U,mu,vars] = pca( X )

% INPUTS
%  X         - [d1 x ... x dm x n], treated as n [d1 x ... x dm] elements
%
% OUTPUTS
%  U         - [d x r], d=prod(di), each column is a principal component
%  mu        - [d1 x ... x dm] mean of X
%  vars      - sorted eigenvalues corresponding to eigenvectors in U

% set X to be zero mean, then flatten
d = size(X);
n = d(end);
d = prod(d(1:end-1));
if ~isa(X,'double')
    X = double(X);
end
if n == 1
    mu = X;
    U = zeros(d,1);
    vars = 0;
    return;
end
mu = mean( X, ndims(X) );
X = bsxfun(@minus,X,mu)/sqrt(n-1);
X = reshape( X, d, n );

% make sure X not too large or SVD slow O(min(d,n)^2.5)
m = 2500;
if min(d,n) > m 
    X = X(:,randperm(n,m));
    n = m; 
end

% get principal components using the SVD of X: X=U*S*V'
if 0
  [U,S] = svd(X,'econ');
  vars = diag(S).^2;
elseif d > n 
  [~,SS,V] = robustSvd(X'*X); 
  vars = diag(SS);
  U = X * V * diag(1./sqrt(vars));
else
  [~,SS,U] = robustSvd(X*X');
  vars = diag(SS);
end

% discard low variance prinicipal components
K = vars>1e-30;
vars = vars(K); 
U = U(:,K);

end

% Robust version of SVD more likely to always converge.
function [U,S,V] = robustSvd( X, trials )

if nargin < 2
    trials = 100;
end
try [U,S,V] = svd(X);
catch
  if trials <= 0
      error('svd did not converge');
  end
  n = numel(X);
  j = randi(n);
  X(j) = X(j)+eps;
  [U,S,V] = robustSvd(X,trials-1);
end
end
