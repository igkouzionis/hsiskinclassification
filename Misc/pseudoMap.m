function classif = pseudoMap(n, m, est_label, name)
dec = reshape(est_label, [n,m]);
classif = label2color(dec);
imshow(classif)
save([name '_pred'], 'classif')
imwrite(classif, [name '_pred.png'], 'png'); 
end