classdef ISODATA_ < Initial_Function
    
    properties (GetAccess = 'public', SetAccess = 'public')
    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        cluster_centers_; % Coordinates of cluster centers
        labels_; % Labels of each point
    end
    
    methods
        function obj = ISODATA_(params)
            if nargin > 0
                obj.set_params(params)
            end
        end
        
        function fit(obj,X, n_clusters, iteration, minimum_n, maximum_variance, minimum_d, ~)
            [centroid, result] = ISODATA(X, iteration, n_clusters, minimum_n, maximum_variance, minimum_d);
            obj.cluster_centers_ = centroid;
            obj.labels_ = result;             
        end
        
        function labels = fit_predict(obj, X, ~)
            obj.fit(X);
            labels = obj.predict(X);
        end
        
        function X_new = fit_transform(obj, X, ~)
            obj.fit(X);
            X_new = obj.transform(X);
        end
        
        function labels = predict(obj, X, ~)
            X_new = obj.transform(X);
            [~,labels] = min(X_new, [], 2);
        end
        
        function X_new = transform(obj, X, ~)
            X_new = dist2(X, obj.cluster_centers_);
        end
    end
end
