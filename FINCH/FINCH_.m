classdef FINCH_ < Initial_Function
    
    properties (GetAccess = 'public', SetAccess = 'public')
        
    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        labels_; % Labels of each point
    end
    
    methods
        function obj = FINCH_(params)
            if nargin > 0
                obj.set_params(params)
            end
        end
        
        function fit(obj, X, initial_rank, verbose, K, ~)
            [c, num_clust] = FINCH(X, initial_rank, verbose);
            for i = 1:size(num_clust,2)
                if num_clust(:,i) < K 
                    num_clust(:,i) = [];
                    c(:,i) = [];
                end
            end
            obj.labels_ = c(:, size(c,2));
        end
        
        function labels = fit_predict(obj, X, ~)
            obj.fit(X);
            labels = obj.predict(X);
        end
        
        function X_new = fit_transform(obj, X, ~)
            obj.fit(X);
            X_new = obj.transform(X);
        end
        
        function labels = predict(obj, X, cluster_centers, ~)
            X_new = obj.transform(X, cluster_centers);
            [~,labels] = min(X_new,[],2);
        end
        
        function X_new = transform(obj, X, cluster_centers, ~)
            X_new = dist2(X, cluster_centers);
        end
    end
end
