function [centroid, class] = Kmeans_InitCluster(data, k, iteration, centroid, distance)

% Args:
%   data: data to be clustered (n * p)
%   k: the number of classes
%   iteration: maximum number of iterations
%
% Returns:
%   centroid: clustering centroids for all classes
%   class: corresponding class for all samples
%
% Distance Metrics used:
% 'SqEuc': squared euclidean
% 'Cos': cosine distance
% 'Mink': minkowski distance
% 'SAM': spectral angle mapper
% 'SID': spectral information divergence
% 'SIDSAM': combination of SID and SAM

% Following steps are same to kmeans
class = zeros(size(data,1),1);
distance_matrix = zeros(size(data,1), k);

for i = 1:iteration
    
    previous_result = class; % for early termination
    
    % Calculate eculidean distance between each sample and each centroid
    for j = 1:size(distance_matrix,1)
        for k = 1:size(distance_matrix,2)
		if strcmp(distance,'SqEuc') 
             		distance_matrix(j,k) = sqrt((data(j,:)-centroid(k,:)) * (data(j,:)-centroid(k,:))');
		elseif strcmp(distance, 'Cos')
             		distance_matrix(j,k) = Cosine(data(j,:),centroid(k,:));
		elseif strcmp(distance, 'Mink')
			distance_matrix(j,k) = minkowskiDistance(data(j,:),centroid(k,:));
		elseif strcmp(distance, 'SAM')
             		distance_matrix(j,k) = SAM(data(j,:),centroid(k,:));
		elseif strcmp(distance, 'SID')
             	distance_matrix(j,k) = SID(data(j,:),centroid(k,:));
		elseif strcmp(distance, 'SISAM')
            		distance_matrix(j,k) = SID_SAM(data(j,:),centroid(k,:));
        end
    end
    
    % Assign each sample to the nearest controid
    [~,class] = min(distance_matrix,[],2);
    
    % Recalculate centroids
    for j = 1:k
        centroid(j,:) = mean(data(class(:,1) == j,:));
    end
    
    % Display
    fprintf('---- %ith iteration completed---- \n',i);
    
    % If classified results on all points do not change after an iteration, 
    % the clustering process will quit immediately.
    if(class == previous_result)
        fprintf('**** Clustering over after %i iterations ****\n',i);
        break;
    end
end
end
end
