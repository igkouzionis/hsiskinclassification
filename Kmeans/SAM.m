function angle = SAM(a, b)

angle = acos(dot(a, b)./ (norm(a).*norm(b)));

end
