classdef KMeans_ < Initial_Function
    
    properties (GetAccess = 'public', SetAccess = 'public')

    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        cluster_centers_; 
        labels_; 
    end
    
    methods
        function obj = KMeans_(params)
            if nargin > 0
                obj.set_params(params)
            end
        end
        
        function fit(obj, X, ncluster, iterations, start, distance, ~)
            
		if strcmp(start, 'InitRandom')      
             		[centroid, class] = Kmeans_InitRandom(X, ncluster, iterations, distance);
             		obj.cluster_centers_ = centroid;
             		obj.labels_ = class;
		
		elseif strcmp(start, 'InitPlus')        
            		[centroid, class] = Kmeans_InitPlusPlus(X, ncluster, iterations, distance);             
            		obj.cluster_centers_ = centroid;
            		obj.labels_ = class;

		elseif strcmp(start, 'InitCluster') 
	             numelements = round(0.1*length(X));
	             indices = randperm(length(X));
	             indices = indices(1:numelements);
	             Xsub = X(indices,:);
	             [centroidsub, classsub] = Kmeans_InitPlusPlus(Xsub, ncluster, iterations, distance);
	             [centroid, class] = Kmeans_InitCluster(X, ncluster, iterations, centroidsub, distance);
	             obj.cluster_centers_ = centroid;
	             obj.labels_ = class;
                        
        end
        end
        
        function labels = fit_predict(obj, X, ~)
            obj.fit(X);
            labels = obj.predict(X);
        end
        
        function X_new = fit_transform(obj, X, ~)
            obj.fit(X);
            X_new = obj.transform(X);
        end
        
        function labels = predict(obj, X, ~)
            X_new = obj.transform(X);
            [~,labels] = min(X_new,[],2);
        end
        
        function X_new = transform(obj, X, ~)
            X_new = dist2(X, obj.cluster_centers_);
        end
    end
end
