function distance = Cosine(x, y)

distance = 1 - (x*y')/sqrt((x*x')*(y*y'));

end
