function result = SID_SAM(a, b)

angle = SAM(a, b);
divergence = SID(a, b);
result = divergence.*(tan(angle));
end
