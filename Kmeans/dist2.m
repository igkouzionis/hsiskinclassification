function n2 = dist2(x, c)

[ndata, dimx] = size(x);
[ncentres, dimc] = size(c);
if dimx ~= dimc
    error('Data dimension does not match dimension of centres')
end

n2 = repmat(sum(x.^2,2),1,ncentres) + ...
    repmat(transpose(sum(c.^2,2)),ndata,1) - ...
    2.*(x*transpose(c));
