function [labels, centroids] = Exec_Kmeans_new(data1, data2, maxK, maxIter, init, dist, flag, color_map)

% data1: low resolution hypercube m*n*d (spatial dimensions m, n and spectral dimension d)
%
% data2: high resolution hypercube m*n*d (spatial dimensions m, n and spectral dimension d)
%
% maxK: maximum number of clusters
%
% maxIter: maximum number of iterations for convergene
%
% init: Initialization method for centroids
% 'InitRandom': random initialization
% 'InitCluster': initialization using a centroids matrix
% 'InitPlus': initialization using K-means++ method
%
% dist: Distance metric
% 'SqEuc': squared euclidean
% 'Cos': cosine distance
% 'Mink': minkowski distance
% 'SAM': spectral angle mapper
% 'SID': spectral information divergence
% 'SIDSAM': combination of SID and SAM

temp1 = mat2gray(data1);
size_temp1 = size(temp1);
temp1 = reshape(permute(temp1,[1 2 3]),[size_temp1(1)*size_temp1(2) size_temp1(3)]);

temp2 = mat2gray(data2);
size_temp2 = size(temp2);
temp2 = reshape(permute(temp2,[1 2 3]),[size_temp2(1)*size_temp2(2) size_temp2(3)]);

%% Perform PCA

[U,mu,vars] = pca(temp2);
pc1 = reshape(U(:,1), [size_temp2(1) size_temp2(2)]);
pc2 = reshape(U(:,2), [size_temp2(1) size_temp2(2)]);
pc3 = reshape(U(:,3), [size_temp2(1) size_temp2(2)]);

imwrite(pc1.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PC1.png'));
imwrite(pc2.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PC2.png'));
imwrite(pc3.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PC3.png'));

PCAImage = cat(3, pc1, pc2, pc3);
imwrite(PCAImage.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PCA_RGB.png'));

%% Determine number of clusters using Elbow method

% kmax = maxK;
% [~,~,~,K]=kmeans_opt(temp1,kmax);

%% Perform Kmeans

n_cluster = maxK;
iterations = maxIter; 
distance = dist;
start = init;

tic % elapsed time

if flag == 1
    % Gaussian Random Projection
    type = 'RP';
    RP = randn(size(temp1,2),maxK)/sqrt(maxK);
    RP = RP./sqrt(sum(RP.^2));
    clf = KMeans_new_();
    clf.fit(temp1*RP, n_cluster, iterations, start, distance); % training step
    labels = clf.labels_;
    centroids = zeros(n_cluster, size_temp1(3));
    % calculate centroids
    for j = 1:n_cluster
            centroids(j,:) = mean(temp1(labels(:,1) == j,:));
    end
    y_est = clf.predict(temp2, centroids); % mapping step
    labels = y_est;
elseif flag == 2
    % Fast ICA
    type = 'fICA';
    ICA = fastica(temp1', 'firsteig',1,'lasteig',maxK, 'verbose','off','stabilization','on');
    clf = KMeans_new_();
    clf.fit(ICA', n_cluster, iterations, start, distance); % training step
    labels = clf.labels_;
    centroids = zeros(n_cluster, size_temp1(3));
    % calculate centroids
    for j = 1:n_cluster
            centroids(j,:) = mean(temp1(labels(:,1) == j,:));
    end
    y_est = clf.predict(temp2, centroids); % mapping step
    labels = y_est;
elseif flag == 3
    % PCA
    type = 'PCA';
    V = GetPC(temp1);
    clf = KMeans_new_();
    clf.fit(temp1*V(:,1:maxK), n_cluster, iterations, start, distance); % training step
    labels = clf.labels_;
    centroids = zeros(n_cluster, size_temp1(3));
    % calculate centroids
    for j = 1:n_cluster
            centroids(j,:) = mean(temp1(labels(:,1) == j,:));
    end
    y_est = clf.predict(temp2, centroids); % mapping step
    labels = y_est;
end

time_KmeansNew = toc; % elapsed time

%% Save labels and centroids

str = ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_' int2str(n_cluster) '_' distance '_' start '_' type  '_Centroids.xlsx'];
xlswrite(str, centroids.*255);
str2 = ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_' int2str(n_cluster) '_' distance '_' start '_' type '_Labels'];
save(str2 ,'labels')
str3 = ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_' int2str(n_cluster) '_' distance '_' start '_' type];
save([str3 '_time'], 'time_KmeansNew');

%% Produce thematic map

IDX = reshape(labels,[size_temp2(1) size_temp2(2)]);
[imgOut] = hyperConvert2Colormap(IDX, color_map);
% imshow(imgOut);

%% Save the pseudocolor map 

strSave=['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_' start '_' distance '_' 'Clusters' int2str(n_cluster) '_' int2str(flag) '_' 'ResIn' int2str(size_temp1(1)) '_' int2str(size_temp1(2)) '_' 'ResOut' int2str(size_temp2(1)) '_' int2str(size_temp2(2)) '_' int2str(str2num(datestr(now,'ddmmyyHHMMSS')))];
imwrite(imgOut, [strSave '.png'], 'png');
