% Compute the principle components of X.

function V = GetPC(X)

[V,EV]=eig(X'*X);

[~,idx]=sort(diag(EV),'descend');

V=V(:,idx);

end