function distance = CorrelationDistance(x, y)

x_t = (1./size(x,2))*sum(x);
y_t = (1./size(y,2))*sum(y);
distance = 1 - ((x-x_t)*(y-y_t)')/((sqrt((x-x_t)*(x-x_t)'))*(sqrt((y-y_t)*(y-y_t)')));

end
