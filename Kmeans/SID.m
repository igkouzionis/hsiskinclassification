function divergence = SID (x, y)

divergence = sum(x.*log(x./y)) + sum(y.*log(y./x));
% p = x./sum(x);
% q = y./sum(y);
% Dab = sum(p.*log(p./q));
% Dba = sum(q.*log(q./p));
% divergence = Dab + Dba;

end
