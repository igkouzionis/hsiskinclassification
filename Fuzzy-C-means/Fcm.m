function [ClusterIm, centers] = Fcm(temp, NumClusts)
data = temp;
[centers,U] = fcm(data, NumClusts);
maxU = max(U);
ClusterIm = zeros(size(temp,1),1);
for itr = 1:NumClusts
    index = find(U(itr,:) == maxU);
      for jtr = 1:size(index,2)
          ClusterIm(index(jtr)) = itr;  
      end
end
