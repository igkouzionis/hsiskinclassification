function [ClusterIm, center] = htfcm(data,Nclust)

expo = 2;		% Exponent for U
max_iter = 100;		% Max. iteration
min_impro = 1e-5;		% Min. improvement

obj_fcn = zeros(max_iter, 1);	% Array for objective function

U = rand(Nclust,size(data,1));
col_sum = sum(U);
U = U./col_sum(ones(Nclust, 1),:);

% Main loop
for i = 1:max_iter
    mf = U.^expo;      
    center = mf*data./(sum(mf,2)*ones(1,size(data,2))); 
    
    dist = zeros(size(center, 1), size(data, 1));
    for k = 1:size(center, 1)
       dist(k, :) = sqrt(sum(((data-ones(size(data, 1), 1)*center(k, :)).^2), 2));
    end
    
    obj_fcn(i) = sum(sum((dist.^2).*mf)); 
    tmp = dist.^(-2/(expo-1));  
    U = tmp./(ones(Nclust, 1)*sum(tmp));
    
    if i > 1
		if abs(obj_fcn(i) - obj_fcn(i-1)) < min_impro, break; end
	end
end

iter_n = i;	% Actual number of iterations 
% obj_fcn(iter_n+1:max_iter) = [];

maxU = max(U);
ClusterIm = zeros(size(data,1),1);
for itr = 1:Nclust
    index = find(U(itr,:) == maxU);
      for jtr = 1:size(index,2)
          ClusterIm(index(jtr)) = itr;  
      end
end




