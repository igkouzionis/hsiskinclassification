function [labels, centroids] = Exec_Kmedoids(data1, data2, K, distance, color_map)

% data1: low resolution hypercube m*n*d (spatial dimensions m, n and spectral dimension d)
%
% data2: high resolution hypercube m*n*d (spatial dimensions m, n and spectral dimension d)
%
% maxK: maximum number of clusters
%
% distance: distance metric
% 'cosine'
% 'sqeuclidean'

temp1 = mat2gray(data1);
size_temp1 = size(temp1);
temp1 = reshape(permute(temp1,[1 2 3]),[size_temp1(1)*size_temp1(2) size_temp1(3)]);

temp2 = mat2gray(data2);
size_temp2 = size(temp2);
temp2 = reshape(permute(temp2,[1 2 3]),[size_temp2(1)*size_temp2(2) size_temp2(3)]);

%% Perform PCA

[U,mu,vars] = pca(temp2);
pc1 = reshape(U(:,1), [size_temp2(1) size_temp2(2)]);
pc2 = reshape(U(:,2), [size_temp2(1) size_temp2(2)]);
pc3 = reshape(U(:,3), [size_temp2(1) size_temp2(2)]);

imwrite(pc1.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PC1.png'));
imwrite(pc2.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PC2.png'));
imwrite(pc3.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PC3.png'));

PCAImage = cat(3, pc1, pc2, pc3);
imwrite(PCAImage.*255, strcat('C:\Users\ioannis\Documents\MATLAB\Results\PCA_RGB.png'));

%% Determine number of clusters using Elbow method

% kmax = maxK; 
% [~,~,~,K]=kmeans_opt(temp1,kmax);

%% Perform K-medoids

n_cluster = K;  
dist = distance; 

tic % elapsed time

clf = KMedoids_();
clf.fit(temp1, n_cluster, dist); % training step
y_est = clf.predict(temp2); % mapping step
centroids = clf.cluster_centers_;
labels = y_est;

time_Kmedoids = toc; % elapsed time

%% Save labels and centroids

str = ['C:\Users\ioannis\Documents\MATLAB\Results\Kmedoids_' int2str(n_cluster) '_' distance '_Centroids.xlsx'];
xlswrite(str, centroids.*255);
str2 = ['C:\Users\ioannis\Documents\MATLAB\Results\Kmedoids_' int2str(n_cluster) '_' distance '_Labels_'];
save(str2 ,'labels')
str3 = ['C:\Users\ioannis\Documents\MATLAB\Results\Kmedoids_' int2str(n_cluster) '_' distance];
save([str3 '_time'], 'time_Kmedoids');

%% Produce thematic map

IDX = reshape(labels,[size_temp2(1) size_temp2(2)]);
[imgOut] = hyperConvert2Colormap(IDX, color_map);
% imshow(imgOut);

%% Save the pseudocolor map for Kmeans

strSave=['C:\Users\ioannis\Documents\MATLAB\Results\Kmedoids_' distance '_Clusters' int2str(n_cluster) '_' 'ResIn' int2str(size_temp1(1)) '_' int2str(size_temp1(2)) '_' 'ResOut' int2str(size_temp2(1)) '_' int2str(size_temp2(2)) '_' int2str(str2num(datestr(now,'ddmmyyHHMMSS')))];
imwrite(imgOut, [strSave '.png'], 'png');
