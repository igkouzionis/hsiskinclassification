classdef KMedoids_ < Initial_Function
       
    properties (GetAccess = 'public', SetAccess = 'public')
        n_init = 1; 
        n_jobs = 1; % The number of jobs to use for the computation. 
    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        cluster_centers_; % Coordinates of cluster centers
        labels_; % Labels of each point 
    end
    
    methods
        function obj = KMedoids_(params)
            if nargin > 0
                obj.set_params(params)
            end
        end
        
        function fit(obj, X, n_clusters, distance, ~)
            [idx, C] = kmedoids(X, n_clusters,...
                'Distance',distance,...
                'Replicates',obj.n_init,...
                'start','plus');
            obj.cluster_centers_ = C;
            obj.labels_ = idx;

	    % [idx, C, ~] = Kmedoids(X, n_clusters, distMetric);
	    % obj.cluster_centers = C;
	    % obj.labels_ = idx;

        end
        
        function labels = fit_predict(obj, X, ~)
            obj.fit(X);
            labels = obj.predict(X);
        end
        
        function X_new = fit_transform(obj, X, ~)
            obj.fit(X);
            X_new = obj.transform(X);
        end
        
        function labels = predict(obj, X, ~)
            X_new = obj.transform(X);
            [~,labels] = min(X_new,[],2);
        end
        
        function X_new = transform(obj, X, ~)
            X_new = dist2(X,obj.cluster_centers_);
        end
    end
end
