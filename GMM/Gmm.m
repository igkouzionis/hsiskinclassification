function [ClusterIm] = Gmm(temp, NumClusts)

data = temp;
GMMmodel = fitgmdist(data,NumClusts);
ClusterIm = cluster(GMMmodel, data);
