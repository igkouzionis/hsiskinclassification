function W = partition_matrix(data,v,k,m)

% ----------- inputs ------------ 
% data: dataset
% v: centroids
% k: number of clusters
% m: fuzzifier coeff 

% ----------- outputs -----------
% W: partition matrix

if m<2
    m=2;
end

DMX=abs(dist(data,v'));

for i=1:size(data,1)
    for j=1:k
        num=DMX(i,j);
        acc=0;
        for l=1:k
            den=DMX(i,l);
            acc=acc+power(num/den,2/(m-1));
        end
        W(i,j)=1/acc;
    end
end
  
end

