classdef Agglomerative_ < Initial_Function
    
    properties (GetAccess = 'public', SetAccess = 'public')
%         n_clusters = 5; 
    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        cluster_centers_; 
        labels_; 
    end
    
    methods
        function obj = Agglomerative_(params)
            if nargin > 0
                obj.set_params(params)
            end
        end
        
        function fit(obj,X, n_clusters, linkageAlg, metric, ~)
            r = al_agglom(X,n_clusters,linkageAlg,metric);
            obj.cluster_centers_ = r.cluster.V;
            obj.labels_ = r.data.cl;             
        end
        
        function labels = fit_predict(obj, X, ~)
            obj.fit(X);
            labels = obj.predict(X);
        end
        
        function X_new = fit_transform(obj, X, ~)
            obj.fit(X);
            X_new = obj.transform(X);
        end
        
        function labels = predict(obj, X, ~)
            X_new = obj.transform(X);
            [~,labels] = min(X_new, [], 2);
        end
        
        function X_new = transform(obj, X, ~)
            X_new = dist2(X, obj.cluster_centers_);
        end
    end
end
