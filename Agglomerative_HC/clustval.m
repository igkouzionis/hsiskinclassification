function r = clustval(data,k,c)

% ----------- inputs ------------ 
% data: MxN-dataset (M: objects, N: features/dimensions)
% k: number of clusters
% c: cluster solution structure (see "algorihms" folder)

% ----------- outputs ----------- 
% r: structure with validity indices (see below)

V=c.cluster.V;
D=c.data.D;
W=c.data.W;
minDe=c.cluster.minDe; 
De=c.cluster.De; 
mnDa=c.cluster.mnDa;
mdDa=c.cluster.mdDa;
sdDa=c.cluster.sdDa;
mass=c.cluster.mass;
cl=c.data.cl;

[M,N]=size(data);

% Dunn's Index (DI)
DI=min(minDe)/max(mnDa);

% Silhouette (S)
S=mean(silhouette(data,cl,'Euclidean'));

% Calinski-Harabasz (CH)
eva = evalclusters(data,cl,'CalinskiHarabasz');
CH=eva.CriterionValues;

% Davies-Bouldin (DB)
eva = evalclusters(data,cl,'DaviesBouldin');
DB=eva.CriterionValues;
        
% partition coefficient (PC)
PC = 1/M*nansum(nansum(W.^2));

% classification entropy (CE)
LW = (W).*log(W);
CE = -1/M*sum(nansum(LW));    

% Xie and Beni's index (XB)
XB = sum((nansum(D.*W.^2))./(M*min(minDe)));

% G overlap family (Gstr, Grex, Gmin)
% [ g ] = Gvalidity(k,De,mdDa,mnDa,sdDa,mass);

r.S = S;
r.DI = DI;    
r.CH = CH;  
r.DB = DB;  
r.PC = PC;
r.CE = CE;    
r.XB = XB;          
% r.Gstr=g.Gstr;
% r.Grex=g.Grex;
% r.Gmin=g.Gmin;
% r.oi_st=g.oi_st;
% r.oi_rx=g.oi_rx;
% r.oi_mn=g.oi_mn;
% r.volratio=g.volratio;

end
