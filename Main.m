
%% Load the high and low resolution spectral cubes
clear all;clc;

[fimgname,pimgname] = uigetfile({'*.mat'},'Select Images','Multiselect','on');
for p = 1:size(fimgname,2)
    temp{p}=load([pimgname fimgname{1,p}]);  
end

% data1 => low resolution cube, data2 => high resolution cube
data1 = mat2gray(temp{1,2}.sequence);
% data2 = mat2gray(temp{1,1}.X);
data2 =mat2gray(temp{1,1}.sequence);

temp1 = data1;
size_temp1 = size(temp1);
temp1 = reshape(permute(temp1,[1 2 3]),[size_temp1(1)*size_temp1(2) size_temp1(3)]);

temp2 = data2;
size_temp2 = size(temp2);
temp2 = reshape(permute(temp2,[1 2 3]),[size_temp2(1)*size_temp2(2) size_temp2(3)]);


clear fimgname pimgname temp p

%% Clustering Procedure

%% Find optimal number of clusters
maxK = 10;
kmax = maxK; 
[~,~,~,K] = kmeans_opt(temp1,kmax);
K
var = cumulativePCA(data1);
color_map = GetColorMap(K);

%% Execute Agglomerative HC
linkageAlg = 'ward'; % could be: average, centroid, complete, median, single, ward, weighted
metric = 'euclidean'; % could be: euclidean, squaredeuclidean, seuclidean, mahalanobis, cityblock, minkowski, chebychev, cosine, correlation, hamming, jaccard, spearman
[labels_Agglom, centroids_Agglom] = Exec_Agglomerative(data1, data2, K, linkageAlg, metric, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Agglom);
save('C:\Users\ioannis\Documents\MATLAB\Results\Agglomerative_Validation','CH','DB','ssb','ssw')

% IDX_Agglom = reshape(labels_Agglom,[size_temp2(1) size_temp2(2)]);
% [imgOut_Agglom] = hyperConvert2Colormap(IDX_Agglom, color_map);
% figure(1);imshow(imgOut_Agglom)
% imwrite(imgOut_Agglom, ['C:\Users\User\Documents\MATLAB\Results\Agglomerative' '.png'], 'png');

%% Execute First Integer Neighbor Clustering Hierarchy (FINCH) Algorithm
% [labels_FINCH, centroids_FINCH] = Exec_FINCH(data1, data2, K);
% 
% data = temp2;
% [DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_FINCH);
% save('C:\Users\ioannis\Documents\MATLAB\Results\FINCH_Validation','CH','DB','ssb','ssw')

% IDX_FINCH = reshape(labels_FINCH,[size_temp2(1) size_temp2(2)]);
% [imgOut_FINCH] = hyperConvert2Colormap(IDX_FINCH, color_map);
% figure(2);imshow(imgOut_FINCH)

%% Execute Hierarchical Non Matrix Factorization (HNMF) Algorithm
algo = 1; % Could be 1, 2, 3
endmembers = [];

[labels_HNMF, centroids_HNMF] = Exec_HNMF(data1, data2, algo, endmembers, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_HNMF);
save('C:\Users\ioannis\Documents\MATLAB\Results\HNMF_Validation','CH','DB','ssb','ssw')

% IDX_HNMF = reshape(labels_HNMF,[size_temp2(1) size_temp2(2)]);
% [imgOut_HNMF] = hyperConvert2Colormap(IDX_HNMF, color_map);
% figure(3);imshow(imgOut_HNMF)

%% Execute Fuzzy C-means
[labels_FCM, centroids_FCM] = Exec_FCM(data1, data2, K, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_FCM);
save('C:\Users\ioannis\Documents\MATLAB\Results\FCM_Validation','CH','DB','ssb','ssw')

% IDX_FCM = reshape(labels_FCM,[size_temp2(1) size_temp2(2)]);
% [imgOut_FCM] = hyperConvert2Colormap(IDX_FCM, color_map);
% figure(4);imshow(imgOut_FCM)

%% Execute GMM
% [labels_GMM, centroids_GMM] = Exec_GMM(data1, data2, K, color_map);
% 
% data = temp2;
% [DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_GMM);
% save('C:\Users\ioannis\Documents\MATLAB\Results\GMM_Validation','CH','DB','ssb','ssw')

% IDX_GMM = reshape(labels_GMM,[size_temp2(1) size_temp2(2)]);
% [imgOut_GMM] = hyperConvert2Colormap(IDX_GMM, color_map);
% figure(5);imshow(imgOut_GMM)

%% Execute ISODATA
maxIter = 100;
minPts = 10;
maxVar = 1;
minDist = 1;
[labels_ISODATA, centroids_ISODATA] = Exec_ISODATA(data1, data2, K, maxIter, minPts, maxVar, minDist, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_ISODATA);
save('C:\Users\ioannis\Documents\MATLAB\Results\ISODATA_Validation','CH','DB','ssb','ssw')

% IDX_ISODATA = reshape(labels_ISODATA,[size_temp2(1) size_temp2(2)]);
% [imgOut_ISODATA] = hyperConvert2Colormap(IDX_ISODATA, color_map);
% figure(6);imshow(imgOut_ISODATA)

%% Execute Spectral Clustering
[labels_Spectral, centroids_Spectral] = Exec_SpectralCl(data1, data2, K, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Spectral);
save('C:\Users\ioannis\Documents\MATLAB\Results\Spectral_Validation','CH','DB','ssb','ssw')

% IDX_Spectral = reshape(labels_Spectral,[size_temp2(1) size_temp2(2)]);
% [imgOut_Spectral] = hyperConvert2Colormap(IDX_Spectral, color_map);
% figure(7);imshow(imgOut_Spectral)

%% Execute DBSCAN
% minPoints = 100;
% [labels_DBSCAN, centroids_DBSCAN] = Exec_DBSCAN(data1, data2, minPoints);
% 
% data = temp2;
% [DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_DBSCAN);
% save('DBSCAN_Validation','CH','DB','ssb','ssw')

%% Execute Self Organizing Map NN
[labels_SOM, centroids_SOM] = Exec_SOM(data1, data2, K, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_SOM);
save('C:\Users\ioannis\Documents\MATLAB\Results\SOM_Validation','CH','DB','ssb','ssw')

% IDX_SOM = reshape(labels_SOM,[size_temp2(1) size_temp2(2)]);
% [imgOut_SOM] = hyperConvert2Colormap(IDX_SOM, color_map);
% figure(8);imshow(imgOut_SOM)

%% Execute K-means with Random Projection, PCA, and Fast ICA
maxIter = 100;
init = 'InitPlus';
dist = 'SqEuc';
flag = 3; % Could be 1, 2, 3 for PR, PCA, and fICA respectively

[labels_Kmeans_New, centroids_Kmeans_New] = Exec_Kmeans_new(data1, data2, K, maxIter, init, dist, flag, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmeans_New);
save(['C:\Users\ioannis\Documents\MATLAB\Results\KmeansNew_Validation_' dist '_' int2str(flag)],'CH','DB','ssb','ssw')

% IDX_Kmeans_New = reshape(labels_Kmeans_New,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmeans_New] = hyperConvert2Colormap(IDX_Kmeans_New, color_map);
% figure(9);imshow(imgOut_Kmeans_New)

%% Execute K-means with Squared Euclidean
maxIter = 100;
init = 'InitPlus';
dist = 'SqEuc';
[labels_Kmeans_SqEuc, centroids_KmeansSqEuc] = Exec_Kmeans(data1, data2, K, maxIter, init, dist, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmeans_SqEuc);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmeansSqEuc_Validation','CH','DB','ssb','ssw')

% IDX_Kmeans_SqEuc = reshape(labels_Kmeans_SqEuc,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmeans_SqEuc] = hyperConvert2Colormap(IDX_Kmeans_SqEuc, color_map);
% figure(10);imshow(imgOut_Kmeans_SqEuc)

%% Execute K-means with Minkowski
maxIter = 100;
init = 'InitPlus';
dist = 'Mink';
[labels_Kmeans_Mink, centroids_KmeansMink] = Exec_Kmeans(data1, data2, K, maxIter, init, dist, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmeans_Mink);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmeansMink_Validation','CH','DB','ssb','ssw')

% IDX_Kmeans_Mink = reshape(labels_Kmeans_Mink,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmeans_Mink] = hyperConvert2Colormap(IDX_Kmeans_Mink, color_map);
% figure(11);imshow(imgOut_Kmeans_Mink)

%% Execute K-means with Cosine
maxIter = 100;
init = 'InitPlus';
dist = 'Cos';
[labels_Kmeans_Cos, centroids_KmeansCos] = Exec_Kmeans(data1, data2, K, maxIter, init, dist, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmeans_Cos);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmeansCos_Validation','CH','DB','ssb','ssw')

% IDX_Kmeans_Cos = reshape(labels_Kmeans_Cos,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmeans_Cos] = hyperConvert2Colormap(IDX_Kmeans_Cos, color_map);
% figure(12);imshow(imgOut_Kmeans_Cos)

%% Execute K-means with Spectral Angle Mapper
maxIter = 100;
init = 'InitPlus';
dist = 'SAM';
[labels_Kmeans_SAM, centroids_KmeansSAM] = Exec_Kmeans(data1, data2, K, maxIter, init, dist, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmeans_SAM);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmeansSAM_Validation','CH','DB','ssb','ssw')

% IDX_Kmeans_SAM = reshape(labels_Kmeans_SAM,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmeans_SAM] = hyperConvert2Colormap(IDX_Kmeans_SAM, color_map);
% figure(13);imshow(imgOut_Kmeans_SAM)

%% Execute K-means with Spectral Information Divergence
maxIter = 100;
init = 'InitPlus';
dist = 'SID';
[labels_Kmeans_SID, centroids_KmeansSID] = Exec_Kmeans(data1, data2, K, maxIter, init, dist, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmeans_SID);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmeansSID_Validation','CH','DB','ssb','ssw')

% IDX_Kmeans_SID = reshape(labels_Kmeans_SID,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmeans_SID] = hyperConvert2Colormap(IDX_Kmeans_SID, color_map);
% figure(14);imshow(imgOut_Kmeans_SID)

%% Execute K-means with SID - SAM
maxIter = 100;
init = 'InitPlus';
dist = 'SIDSAM';
[labels_Kmeans_SIDSAM, centroids_KmeansSIDSAM] = Exec_Kmeans(data1, data2, K, maxIter, init, dist, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmeans_SIDSAM);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmeansSIDSAM_Validation','CH','DB','ssb','ssw')

% IDX_Kmeans_SIDSAM = reshape(labels_Kmeans_SIDSAM,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmeans_SIDSAM] = hyperConvert2Colormap(IDX_Kmeans_SIDSAM, color_map);
% figure(14);imshow(imgOut_Kmeans_SIDSAM)

%% Execute K-medoids with Squared Euclidean
distance = 'sqeuclidean';
[labels_Kmedoids_SqEuc, centroids_KmedoidsSqEuc] = Exec_Kmedoids(data1, data2, K, distance, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmedoids_SqEuc);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmedoidsSqEuc_Validation','CH','DB','ssb','ssw')

% IDX_Kmedoids_SqEuc = reshape(labels_Kmedoids_SqEuc,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmedoids_SqEuc] = hyperConvert2Colormap(IDX_Kmedoids_SqEuc, color_map);
% figure(15);imshow(imgOut_Kmedoids_SqEuc)

%% Execute K-medoids with Cosine
distance = 'cosine';
[labels_Kmedoids_Cos, centroids_KmedoidsCos] = Exec_Kmedoids(data1, data2, K, distance, color_map);

data = temp2;
[DB,CH,KL,Han,st,sw,sb,cintra,cinter,ssw,ssb] = valid_internal_deviation(data,labels_Kmedoids_Cos);
save('C:\Users\ioannis\Documents\MATLAB\Results\KmedoidsCos_Validation','CH','DB','ssb','ssw')

% IDX_Kmedoids_Cos = reshape(labels_Kmedoids_Cos,[size_temp2(1) size_temp2(2)]);
% [imgOut_Kmedoids_Cos] = hyperConvert2Colormap(IDX_Kmedoids_Cos, color_map);
% figure(16);imshow(imgOut_Kmedoids_Cos)

%% Pause
fprintf('Program paused. Press enter to continue.\n');
pause;

%% Produce Pseudocolor Maps for each Clustering Algorithm
color_map=GetColorMap(K);

IDX_Agglom = reshape(labels_Agglom,[size_temp2(1) size_temp2(2)]);
[imgOut_Agglom] = hyperConvert2Colormap(IDX_Agglom, color_map);
imwrite(imgOut_Agglom, ['C:\Users\ioannis\Documents\MATLAB\Results\Agglomerative' '.png'], 'png');

% cmapFINCH = sameCmap(centroids_Agglom, centroids_FINCH, color_map);
% IDX_FINCH = reshape(labels_FINCH,[size_temp2(1) size_temp2(2)]);
% [imgOut_FINCH] = hyperConvert2Colormap(IDX_FINCH, cmapFINCH);
% imwrite(imgOut_FINCH, ['C:\Users\ioannis\Documents\MATLAB\Results\FINCH' '.png'], 'png');

cmapHNMF = sameCmap(centroids_Agglom, centroids_HNMF, color_map);
IDX_HNMF = reshape(labels_HNMF,[size_temp2(1) size_temp2(2)]);
[imgOut_HNMF] = hyperConvert2Colormap(IDX_HNMF, color_map);
imwrite(imgOut_HNMF, ['C:\Users\ioannis\Documents\MATLAB\Results\HNMF' '.png'], 'png');

cmapFCM = sameCmap(centroids_Agglom, centroids_FCM, color_map);
IDX_FCM = reshape(labels_FCM,[size_temp2(1) size_temp2(2)]);
[imgOut_FCM] = hyperConvert2Colormap(IDX_FCM, cmapFCM);
imwrite(imgOut_FCM, ['C:\Users\ioannis\Documents\MATLAB\Results\FCM' '.png'], 'png');

% cmapGMM = sameCmap(centroids_Agglom, centroids_GMM, color_map);
% IDX_GMM = reshape(labels_GMM,[size_temp2(1) size_temp2(2)]);
% [imgOut_GMM] = hyperConvert2Colormap(IDX_GMM, cmapGMM);
% imwrite(imgOut_GMM, ['C:\Users\ioannis\Documents\MATLAB\Results\GMM' '.png'], 'png');

cmapISODATA = sameCmap(centroids_Agglom, centroids_ISODATA, color_map);
IDX_ISODATA = reshape(labels_ISODATA,[size_temp2(1) size_temp2(2)]);
[imgOut_ISODATA] = hyperConvert2Colormap(IDX_ISODATA, cmapISODATA);
imwrite(imgOut_ISODATA, ['C:\Users\ioannis\Documents\MATLAB\Results\ISODATA' '.png'], 'png');

cmapSpectral = sameCmap(centroids_Agglom, centroids_Spectral, color_map);
IDX_Spectral = reshape(labels_Spectral,[size_temp2(1) size_temp2(2)]);
[imgOut_Spectral] = hyperConvert2Colormap(IDX_Spectral, cmapSpectral);
imwrite(imgOut_Spectral, ['C:\Users\ioannis\Documents\MATLAB\Results\Spectral' '.png'], 'png');

% cmapDBSCAN = sameCmap(centroids_Agglom, centroids_DBSCAN, color_map);
% IDX_DBSCAN = reshape(labels_DBSCAN,[size_temp2(1) size_temp2(2)]);
% [imgOut_DBSCAN] = hyperConvert2Colormap(IDX_DBSCAN, cmapDBSCAN);
% imwrite(imgOut_DBSCAN, ['DBSCAN' '.png'], 'png');

cmapSOM = sameCmap(centroids_Agglom, centroids_SOM, color_map);
IDX_SOM = reshape(labels_SOM,[size_temp2(1) size_temp2(2)]);
[imgOut_SOM] = hyperConvert2Colormap(IDX_SOM, cmapSOM);
imwrite(imgOut_SOM, ['C:\Users\ioannis\Documents\MATLAB\Results\SOM' '.png'], 'png');

cmapKmeansNew = sameCmap(centroids_Agglom, centroids_Kmeans_New, color_map);
IDX_Kmeans_New = reshape(labels_Kmeans_New,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmeans_New] = hyperConvert2Colormap(IDX_Kmeans_New, cmapKmeansNew);
imwrite(imgOut_Kmeans_New, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_New' '.png'], 'png');

cmapKmeansSqEuc = sameCmap(centroids_Agglom, centroids_KmeansSqEuc, color_map);
IDX_Kmeans_SqEuc = reshape(labels_Kmeans_SqEuc,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmeans_SqEuc] = hyperConvert2Colormap(IDX_Kmeans_SqEuc, cmapKmeansSqEuc);
imwrite(imgOut_Kmeans_SqEuc, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_SqEuc' '.png'], 'png');

cmapKmeansMink = sameCmap(centroids_Agglom, centroids_KmeansMink, color_map);
IDX_Kmeans_Mink = reshape(labels_Kmeans_Mink,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmeans_Mink] = hyperConvert2Colormap(IDX_Kmeans_Mink, cmapKmeansMink);
imwrite(imgOut_Kmeans_Mink, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_Mink' '.png'], 'png');

cmapKmeansCos = sameCmap(centroids_Agglom, centroids_KmeansCos, color_map);
IDX_Kmeans_Cos = reshape(labels_Kmeans_Cos,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmeans_Cos] = hyperConvert2Colormap(IDX_Kmeans_Cos, cmapKmeansCos);
imwrite(imgOut_Kmeans_Cos, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_Cos' '.png'], 'png');

cmapKmeansSAM = sameCmap(centroids_Agglom, centroids_KmeansSAM, color_map);
IDX_Kmeans_SAM = reshape(labels_Kmeans_SAM,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmeans_SAM] = hyperConvert2Colormap(IDX_Kmeans_SAM, cmapKmeansSAM);
imwrite(imgOut_Kmeans_SAM, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_SAM' '.png'], 'png');

cmapKmeansSID = sameCmap(centroids_Agglom, centroids_KmeansSID, color_map);
IDX_Kmeans_SID = reshape(labels_Kmeans_SID,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmeans_SID] = hyperConvert2Colormap(IDX_Kmeans_SID, cmapKmeansSID);
imwrite(imgOut_Kmeans_SID, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_SID' '.png'], 'png');

cmapKmeansSIDSAM = sameCmap(centroids_Agglom, centroids_KmeansSIDSAM, color_map);
IDX_Kmeans_SIDSAM = reshape(labels_Kmeans_SIDSAM,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmeans_SIDSAM] = hyperConvert2Colormap(IDX_Kmeans_SIDSAM, cmapKmeansSIDSAM);
imwrite(imgOut_Kmeans_SIDSAM, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmeans_SIDSAM' '.png'], 'png');

cmapKmedoidsSqEuc = sameCmap(centroids_Agglom, centroids_KmedoidsSqEuc, color_map);
IDX_Kmedoids_SqEuc = reshape(labels_Kmedoids_SqEuc,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmedoids_SqEuc] = hyperConvert2Colormap(IDX_Kmedoids_SqEuc, cmapKmedoidsSqEuc);
imwrite(imgOut_Kmedoids_SqEuc, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmedoids_SqEuc' '.png'], 'png');

cmapKmedoidsCos = sameCmap(centroids_Agglom, centroids_KmedoidsCos, color_map);
IDX_Kmedoids_Cos = reshape(labels_Kmedoids_Cos,[size_temp2(1) size_temp2(2)]);
[imgOut_Kmedoids_Cos] = hyperConvert2Colormap(IDX_Kmedoids_Cos, cmapKmedoidsCos);
imwrite(imgOut_Kmedoids_Cos, ['C:\Users\ioannis\Documents\MATLAB\Results\Kmedoids_Cos' '.png'], 'png');

% % %% Pause
% % fprintf('Program paused. Press enter to continue.\n');
% % pause;
% % 
% % 
% % 
% % %% Pause
% % fprintf('Program paused. Press enter to continue.\n');
% % pause;
% % 
% % %% Produce Pseudocolor Maps for each Clustering Algorithm
% % cmapAgglom = sameCmap_2(centroids_Agglom, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Agglom = reshape(labels_Agglom,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Agglom] = hyperConvert2Colormap(IDX_Agglom, cmapAgglom);
% % imwrite(imgOut_Agglom, ['Agglomerative' '.png'], 'png');
% % 
% % cmapFCM = sameCmap_2(centroids_FCM, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_FCM = reshape(labels_FCM,[size_temp2(1) size_temp2(2)]);
% % [imgOut_FCM] = hyperConvert2Colormap(IDX_FCM, cmapFCM);
% % imwrite(imgOut_FCM, ['FCM' '.png'], 'png');
% % 
% % cmapGMM = sameCmap_2(centroids_GMM, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_GMM = reshape(labels_GMM,[size_temp2(1) size_temp2(2)]);
% % [imgOut_GMM] = hyperConvert2Colormap(IDX_GMM, cmapGMM);
% % imwrite(imgOut_GMM, ['GMM' '.png'], 'png');
% % 
% % cmapISODATA = sameCmap_2(centroids_ISODATA, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_ISODATA = reshape(labels_ISODATA,[size_temp2(1) size_temp2(2)]);
% % [imgOut_ISODATA] = hyperConvert2Colormap(IDX_ISODATA, cmapISODATA);
% % imwrite(imgOut_ISODATA, ['ISODATA' '.png'], 'png');
% % 
% % cmapSpectral = sameCmap_2(centroids_Spectral, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Spectral = reshape(labels_Spectral,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Spectral] = hyperConvert2Colormap(IDX_Spectral, cmapSpectral);
% % imwrite(imgOut_Spectral, ['Spectral' '.png'], 'png');
% % 
% % % cmapDBSCAN = sameCmap(centroids_Agglom, centroids_DBSCAN, color_map);
% % % IDX_DBSCAN = reshape(labels_DBSCAN,[size_temp2(1) size_temp2(2)]);
% % % [imgOut_DBSCAN] = hyperConvert2Colormap(IDX_DBSCAN, cmapDBSCAN);
% % % imwrite(imgOut_DBSCAN, ['DBSCAN' '.png'], 'png');
% % 
% % cmapSOM = sameCmap_2(centroids_SOM, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_SOM = reshape(labels_SOM,[size_temp2(1) size_temp2(2)]);
% % [imgOut_SOM] = hyperConvert2Colormap(IDX_SOM, cmapSOM);
% % imwrite(imgOut_SOM, ['SOM' '.png'], 'png');
% % 
% % cmapKmeansSqEuc = sameCmap_2(centroids_KmeansSqEuc, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmeans_SqEuc = reshape(labels_Kmeans_SqEuc,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmeans_SqEuc] = hyperConvert2Colormap(IDX_Kmeans_SqEuc, cmapKmeansSqEuc);
% % imwrite(imgOut_Kmeans_SqEuc, ['Kmeans_SqEuc' '.png'], 'png');
% % 
% % cmapKmeansMink = sameCmap_2(centroids_KmeansMink, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmeans_Mink = reshape(labels_Kmeans_Mink,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmeans_Mink] = hyperConvert2Colormap(IDX_Kmeans_Mink, cmapKmeansMink);
% % imwrite(imgOut_Kmeans_Mink, ['Kmeans_Mink' '.png'], 'png');
% % 
% % cmapKmeansCos = sameCmap_2(centroids_KmeansCos, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmeans_Cos = reshape(labels_Kmeans_Cos,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmeans_Cos] = hyperConvert2Colormap(IDX_Kmeans_Cos, cmapKmeansCos);
% % imwrite(imgOut_Kmeans_Cos, ['Kmeans_Cos' '.png'], 'png');
% % 
% % cmapKmeansSAM = sameCmap_2(centroids_KmeansSAM, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmeans_SAM = reshape(labels_Kmeans_SAM,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmeans_SAM] = hyperConvert2Colormap(IDX_Kmeans_SAM, cmapKmeansSAM);
% % imwrite(imgOut_Kmeans_SAM, ['Kmeans_SAM' '.png'], 'png');
% % 
% % cmapKmeansSID = sameCmap_2(centroids_KmeansSID, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmeans_SID = reshape(labels_Kmeans_SID,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmeans_SID] = hyperConvert2Colormap(IDX_Kmeans_SID, cmapKmeansSID);
% % imwrite(imgOut_Kmeans_SID, ['Kmeans_SID' '.png'], 'png');
% % 
% % cmapKmeansSIDSAM = sameCmap_2(centroids_KmeansSIDSAM, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmeans_SIDSAM = reshape(labels_Kmeans_SIDSAM,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmeans_SIDSAM] = hyperConvert2Colormap(IDX_Kmeans_SIDSAM, cmapKmeansSIDSAM);
% % imwrite(imgOut_Kmeans_SIDSAM, ['Kmeans_SIDSAM' '.png'], 'png');
% % 
% % cmapKmedoidsSqEuc = sameCmap_2(centroids_KmedoidsSqEuc, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmedoids_SqEuc = reshape(labels_Kmedoids_SqEuc,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmedoids_SqEuc] = hyperConvert2Colormap(IDX_Kmedoids_SqEuc, cmapKmedoidsSqEuc);
% % imwrite(imgOut_Kmedoids_SqEuc, ['Kmedoids_SqEuc' '.png'], 'png');
% % 
% % cmapKmedoidsCos = sameCmap_2(centroids_KmedoidsCos, comp_spec, dysp_spec, junc_spec, mel_spec, norm_spec);
% % IDX_Kmedoids_Cos = reshape(labels_Kmedoids_Cos,[size_temp2(1) size_temp2(2)]);
% % [imgOut_Kmedoids_Cos] = hyperConvert2Colormap(IDX_Kmedoids_Cos, cmapKmedoidsCos);
% % imwrite(imgOut_Kmedoids_Cos, ['Kmedoids_Cos' '.png'], 'png');

%% Pause
% fprintf('Program paused. Press enter to continue.\n');
% pause;
% 
% figure(1)
% imshow(imgOut_Agglom)
% figure(2)
% imshow(imgOut_FCM)
% % figure(3)
% % imshow(imgOut_GMM)
% figure(4)
% imshow(imgOut_ISODATA)
% figure(5)
% imshow(imgOut_SOM)
% figure(6)
% imshow(imgOut_Spectral)
% % figure(7)
% % imshow(imgOut_DBSCAN)
% figure(8)
% imshow(imgOut_Kmeans_SqEuc)
% figure(9)
% imshow(imgOut_KmeansMink)
% figure(10)
% imshow(imgOut_Kmeans_Cos)
% figure(11)
% imshow(imgOut_Kmeans_SAM)
% figure(12)
% imshow(imgOut_Kmeans_SID)
% figure(13)
% imshow(imgOut_Kmeans_SIDSAM)
% figure(14)
% imshow(imgOut_Kmedoids_SqEuc)
% figure(15)
% imshow(imgOut_Kmedoids_Cos)
