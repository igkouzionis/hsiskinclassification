classdef SOM_ < Initial_Function
    
    properties (GetAccess = 'public', SetAccess = 'public')
        
    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        labels_; % Labels of each point
    end
    
    methods
        function obj = SOM_(params)
            if nargin > 0
                obj.set_params(params)
            end
        end
        
        function fit(obj, X, n_cluster, ~)
            idx = SOM(X, n_cluster);
            obj.labels_ = idx;
        end
        
        function labels = fit_predict(obj, X, ~)
            obj.fit(X);
            labels = obj.predict(X);
        end
        
        function X_new = fit_transform(obj, X, ~)
            obj.fit(X);
            X_new = obj.transform(X);
        end
        
        function labels = predict(obj, X, cluster_centers, ~)
            X_new = obj.transform(X, cluster_centers);
            [~,labels] = min(X_new,[],2);
        end
        
        function X_new = transform(obj, X, cluster_centers, ~)
            X_new = dist2(X, cluster_centers);
        end
    end
end
