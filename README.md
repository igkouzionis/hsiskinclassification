# README #

This repository contains a part of the research work done towards my MSc degree in EEE.
The concept behind it is the classification of hyperspectral images from skin lesions with tha final aim of skin cancer diagnosis. 
Methods for feature selection, dimensionality reduction, unsupervised classification (clustering) and supervised classification are applied. Due to the absence of labelled (ground truth) data,
we decided to use clustering methods to find the different classes that exist into the hyperspectral images based on reflectance (grey value) information.
Once the clustering done, experienced clinicians interpret them and then a ground truth produced based on the clustering pseudocolour maps.
Finally, supervised classification methods applied on a new set of hyperspectral images of skin lesions.

More information you can find in the report and presentation.
Do not hesitate to contact me at ioannis.gkouzionis@gmail.com if you have any further questions.